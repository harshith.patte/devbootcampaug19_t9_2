package com.bootcamp.evolutionoftrust;

import java.util.Scanner;

public interface Behaviour {
    Move move();
}