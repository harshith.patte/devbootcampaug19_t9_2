package com.bootcamp.evolutionoftrust;

import java.util.Observable;
import java.util.Observer;

public class CopyCatBehaviour implements Behaviour, Observer {

    private Move otherPlayersMove = null;
    private Move myPreviousMove = null;

    @Override
    public Move move() {
        Move move =  otherPlayersMove == null ?  Move.COOPERATE: otherPlayersMove;
        myPreviousMove = move;
        return move;
    }

    @Override
    public void update(Observable observable, Object o) {
        if(o instanceof Move[]) {

            Move[] moves= ((Move[]) o);

            if(moves[0] != myPreviousMove) {
                otherPlayersMove = moves[0];
            } else if(moves[1] != myPreviousMove) {
                otherPlayersMove = moves[1];
            }
        }
    }

    public void startListenToGameMoves(Game game) {
        game.addObserver(this);
    }
}
