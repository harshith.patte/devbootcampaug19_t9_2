package com.bootcamp.evolutionoftrust;

import java.util.ArrayList;
import java.util.List;

public class DetectiveBehaviour implements Behaviour{

    private List<Move> detectiveInitialMoveArray;

    DetectiveBehaviour(){
        detectiveInitialMoveArray = new ArrayList<>();
        detectiveInitialMoveArray.add(Move.COOPERATE);
        detectiveInitialMoveArray.add(Move.COOPERATE);
        detectiveInitialMoveArray.add(Move.CHEAT);
        detectiveInitialMoveArray.add(Move.COOPERATE);
    }

    @Override
    public Move move() {
        return getDetectiveInitialMove();
    }

    public Move getDetectiveInitialMove() {
        return detectiveInitialMoveArray.remove(detectiveInitialMoveArray.size() - 1);
    }
}
