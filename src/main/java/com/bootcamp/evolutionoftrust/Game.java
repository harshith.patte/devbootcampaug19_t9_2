package com.bootcamp.evolutionoftrust;

import java.io.PrintStream;
import java.util.Observable;

public class Game extends Observable {

    private Player playerOne;
    private Player playerTwo;
    private Machine machine;
    private int rounds;
    private PrintStream printStream;

    public Game(Player playerOne, Player playerTwo, Machine machine, int rounds, PrintStream printStream) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.machine = machine;
        this.rounds = rounds;
        this.printStream = printStream;
    }

    public void start() {
        for (int i = 0; i < rounds; i++) {
            Move playerOneMove = playerOne.move();
            Move playerTwoMove = playerTwo.move();

            String[] scores = machine.calculateScore(playerOneMove, playerTwoMove)
                    .split(",");

            playerOne.updateScore(Integer.parseInt(scores[0]));
            playerTwo.updateScore(Integer.parseInt(scores[1]));
            printStream.println(String.format("Player One: %s, Player Two: %s", playerOne.score(), playerTwo.score()));

            exposeMoves(playerOneMove, playerTwoMove);
        }
    }

    private void exposeMoves(Move playerOneMove, Move playerTwoMove) {
        setChanged();
        notifyObservers(new Move[]{playerOneMove, playerTwoMove});
    }
}
