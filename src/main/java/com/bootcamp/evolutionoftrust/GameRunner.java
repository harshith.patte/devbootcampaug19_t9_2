package com.bootcamp.evolutionoftrust;

import java.io.PrintStream;
import java.util.Scanner;

public class GameRunner {

    public static void main(String [] args) {
        Player consolePlayer = new Player(()-> Move.valueOf(new Scanner(System.in).nextLine()));
        Player cheater = new Player(()-> Move.valueOf("CHEAT"));
        Player cooperate = new Player(()-> Move.valueOf("COOPERATE"));
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        CopyCatBehaviour copyCatBehaviour2 = new CopyCatBehaviour();
        Player copyCatPlayer = new Player(copyCatBehaviour);
        Player copyCatPlayer2 = new Player(copyCatBehaviour2);

        GrudgerBehaviour grudgerBehaviour = new GrudgerBehaviour();
        Player grudgerPlayer = new Player(grudgerBehaviour);



        Machine machine = new Machine();
        PrintStream console = System.out;
        Game game = new Game(grudgerPlayer, cheater, machine, 5, console);


        game.addObserver(copyCatBehaviour);
        game.addObserver(copyCatBehaviour2);
        game.addObserver(grudgerBehaviour);

        game.start();
    }
}
