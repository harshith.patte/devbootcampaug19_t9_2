package com.bootcamp.evolutionoftrust;

import java.util.Observable;
import java.util.Observer;

public class GrudgerBehaviour implements Behaviour, Observer {

    private Move grudgersMove = Move.COOPERATE;

    @Override
    public Move move() {
        return grudgersMove;
    }

    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof Move[]) {
            Move[] moves = ((Move[]) arg);

            if (moves[0] == Move.CHEAT || moves[1] == Move.CHEAT) {
                grudgersMove = Move.CHEAT;
            }
        }
    }
}
