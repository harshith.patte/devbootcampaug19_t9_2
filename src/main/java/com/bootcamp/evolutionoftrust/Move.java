package com.bootcamp.evolutionoftrust;

public enum Move {
    COOPERATE,
    CHEAT
}
