package com.bootcamp.evolutionoftrust;

public class Player {
    private Behaviour behaviour;
    private int score;

    public Player(Behaviour behavior) {
        this.behaviour = behavior;
    }

    public Player() { }

    public Move move() {
        return behaviour.move();
    }

    public int score() {
        return this.score;
    }

    public void updateScore(int score) {
        this.score += score;
    }
}
