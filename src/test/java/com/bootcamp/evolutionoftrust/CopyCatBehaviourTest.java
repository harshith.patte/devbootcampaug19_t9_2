package com.bootcamp.evolutionoftrust;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class CopyCatBehaviourTest {

    CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();

    @Test
    public void testCopyCatReturnsCooperateAtFirst() {
        Assert.assertEquals(copyCatBehaviour.move(), Move.COOPERATE);
    }

    @Test
    public void shouldListenToObservable() {
        Game game = Mockito.mock(Game.class);
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        copyCatBehaviour.startListenToGameMoves(game);

        Mockito.verify(game).addObserver(copyCatBehaviour);
    }

    @Test
    public void shouldReturnCopiedBehaviour(){
        Game observer = Mockito.mock(Game.class);
        copyCatBehaviour.move();
        copyCatBehaviour.update(observer,new Move[]{Move.COOPERATE,Move.CHEAT});
        Assert.assertEquals(copyCatBehaviour.move(), Move.CHEAT);
    }
}
