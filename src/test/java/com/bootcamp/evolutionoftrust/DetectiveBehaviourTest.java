package com.bootcamp.evolutionoftrust;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DetectiveBehaviourTest {

    DetectiveBehaviour detectiveBehaviour;

    @Before
    public void setUp() {
        detectiveBehaviour = new DetectiveBehaviour();
    }

    @Test
    public void intializeDetectiveBehaviour(){
        Assert.assertNotNull(detectiveBehaviour);
    }

    // Testing the intial values
    @Test
    public void shouldReturnCorrectDetectiveBehaviour(){
        Assert.assertEquals(detectiveBehaviour.move(), Move.COOPERATE);
        Assert.assertEquals(detectiveBehaviour.move(), Move.CHEAT);
        Assert.assertEquals(detectiveBehaviour.move(), Move.COOPERATE);
        Assert.assertEquals(detectiveBehaviour.move(), Move.COOPERATE);
    }

    @Test
    public void shouldFailCorrectReturnDetectiveValue() {
        Assert.assertNotEquals(detectiveBehaviour.move(), Move.CHEAT);
        Assert.assertNotEquals(detectiveBehaviour.move(), Move.COOPERATE);
        Assert.assertNotEquals(detectiveBehaviour.move(), Move.CHEAT);
        Assert.assertNotEquals(detectiveBehaviour.move(), Move.CHEAT);

    }

    // Check the state
    @Test
    public void shouldCheckOtherPlayerCheated() {

    }


}
