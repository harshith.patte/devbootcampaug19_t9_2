package com.bootcamp.evolutionoftrust;

import org.junit.Assert;
import org.junit.Test;

import java.io.PrintStream;
import java.util.Scanner;

public class GameIntegrationTest {

    @Test
    public void testCopyCatGettingCheatOfOtherPlayerFromGame() {
        Player cheater = new Player(()-> Move.valueOf("CHEAT"));
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        Machine machine = new Machine();
        PrintStream console = System.out;
        Player copyCatPlayer = new Player(copyCatBehaviour);
        Game game = new Game(copyCatPlayer, cheater, machine, 1, console);

        game.addObserver(copyCatBehaviour);
        game.start();

        Assert.assertEquals(copyCatBehaviour.move(), Move.CHEAT);
    }
}
