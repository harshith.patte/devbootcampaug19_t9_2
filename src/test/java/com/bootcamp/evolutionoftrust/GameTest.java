package com.bootcamp.evolutionoftrust;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class GameTest {
    private Player playerOne;
    private Player playerTwo;
    private Machine machine;
    private PrintStream printStream;

    private Game game;

    @Before
    public void setup() {
        playerOne = mock(Player.class);
        playerTwo = mock(Player.class);
        machine = mock(Machine.class);
        printStream = mock(PrintStream.class);

        when(playerOne.move()).thenReturn(Move.COOPERATE);
        when(playerTwo.move()).thenReturn(Move.CHEAT);
        when(machine.calculateScore(any(), any())).thenReturn("-1,3");

        game = new Game(playerOne, playerTwo, machine, 1, printStream);
    }

    @Test
    public void shouldStartGameWithOneRound() {
        // Arrange
        when(playerOne.score()).thenReturn(-1);
        when(playerTwo.score()).thenReturn(3);

        // Act
        game.start();

        // Asserts
        verify(playerOne).move();
        verify(playerTwo).move();
        verify(machine).calculateScore(Move.COOPERATE, Move.CHEAT);
        verify(printStream, times(1)).println("Player One: -1, Player Two: 3");
    }

    @Test
    public void shouldStartGameWithTwoRound() {
        // Arrange
        game = new Game(playerOne, playerTwo, machine, 2, printStream);
        when(playerOne.score()).thenReturn(-1).thenReturn(-2);
        when(playerTwo.score()).thenReturn(3).thenReturn(6);

        // Act
        game.start();

        // Asserts
        verify(playerOne, times(2)).move();
        verify(playerTwo, times(2)).move();
        verify(machine, times(2)).calculateScore(Move.COOPERATE, Move.CHEAT);
        verify(printStream).println("Player One: -1, Player Two: 3");
        verify(printStream).println("Player One: -2, Player Two: 6");
    }

    @Test
    public void shouldGameNotifyMoves() {
        // Arrange

        CopyCatBehaviour copyCatBehaviour = Mockito.mock(CopyCatBehaviour.class);

        game = new Game(playerOne, playerTwo, machine, 1, printStream);
        when(playerOne.score()).thenReturn(-1).thenReturn(-2);

        // Act
        game.addObserver(copyCatBehaviour);
        game.start();

        // Asserts
        verify(copyCatBehaviour, times(1)).update(game, new Move[]{Move.COOPERATE, Move.CHEAT});
    }

}
