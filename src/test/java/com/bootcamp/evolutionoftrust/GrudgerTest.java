package com.bootcamp.evolutionoftrust;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class GrudgerTest {

    GrudgerBehaviour grudgerBehaviour;

    @Before
    public void setup(){
        grudgerBehaviour = new GrudgerBehaviour();
    }

    @Test
    public void getInstanceOfGrudgerBehaviour() {
        Assert.assertNotNull(grudgerBehaviour);
    }

    @Test
    public void shouldReturnCooperate(){
        Assert.assertEquals(grudgerBehaviour.move(), Move.COOPERATE);
    }

    @Test
    public void shouldReturnCheatOnceMeetCheat() {
        Game observer = Mockito.mock(Game.class);
        grudgerBehaviour.move();
        grudgerBehaviour.update(observer,new Move[]{Move.COOPERATE,Move.CHEAT});
        Assert.assertEquals(grudgerBehaviour.move(), Move.CHEAT);
    }

}
