package com.bootcamp.evolutionoftrust;

import org.junit.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class IntegrationTest {
    private PrintStream printStream = mock(PrintStream.class);

    @Test
    public void fiveRoundsOfGameWithCheaterAndCoolPlayer() {
        Player cheater = new Player(()-> Move.valueOf("CHEAT"));
        Player coolPlayer = new Player(()-> Move.valueOf("COOPERATE"));
        Game game = new Game(cheater, coolPlayer, new Machine(), 5, printStream);

        game.start();

        verify(printStream).println("Player One: 3, Player Two: -1");
        verify(printStream).println("Player One: 6, Player Two: -2");
        verify(printStream).println("Player One: 9, Player Two: -3");
        verify(printStream).println("Player One: 12, Player Two: -4");
        verify(printStream).println("Player One: 15, Player Two: -5");
    }

}
