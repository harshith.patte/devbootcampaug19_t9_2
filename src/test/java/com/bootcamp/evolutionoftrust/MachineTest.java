package com.bootcamp.evolutionoftrust;

import org.junit.Assert;
import org.junit.Test;

public class MachineTest {

    private Machine machine = new Machine();

    @Test
    public void bothPlayersCooperateAndGetsTwoPointsEach() {
        String scores = machine.calculateScore(Move.COOPERATE, Move.COOPERATE);
        Assert.assertEquals("2,2", scores);
    }

    @Test
    public void bothPlayersCheatsAndGetsNothing() {
        Assert.assertEquals("0,0", machine.calculateScore(Move.CHEAT, Move.CHEAT));
    }

    @Test
    public void playerOneCooperatesAndPlayerTwoCheatsAndPlayerOneLosesAPointAndPlayerTwoGainsThreePoints() {
        Assert.assertEquals("-1,3", machine.calculateScore(Move.COOPERATE, Move.CHEAT));
    }

    @Test
    public void playerOneCheatsAndPlayerTwoCooperatesAndPlayerGainsThreePointsAPointAndPlayerTwoLosesPoints() {
        Assert.assertEquals("3,-1", machine.calculateScore(Move.CHEAT, Move.COOPERATE));
    }
}
