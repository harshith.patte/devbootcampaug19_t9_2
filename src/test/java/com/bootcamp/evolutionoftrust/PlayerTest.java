package com.bootcamp.evolutionoftrust;
import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

import static org.mockito.Mockito.mock;

public class PlayerTest {

    Player player = new Player();

    @Test
    public void playerMakesACooperateMove() {
        player = new Player(()-> Move.valueOf(new Scanner("COOPERATE").nextLine()));
        Assert.assertEquals(Move.COOPERATE, player.move());
    }

    @Test
    public void playerMakesACheatMove() {
        player = new Player(()-> Move.valueOf(new Scanner("CHEAT").nextLine()));
        Assert.assertEquals(Move.CHEAT, player.move());
    }

    @Test
    public void playerAlwaysMakeACheatMove() {
        player = new Player(()-> Move.valueOf("CHEAT"));
        Assert.assertEquals(Move.CHEAT, player.move());
    }

    @Test
    public void playerAlwaysCooperates() {
        player = new Player(()-> Move.valueOf("COOPERATE"));
        Assert.assertEquals(Move.COOPERATE, player.move());
    }

    @Test
    public void playerGainScore() {
        player.updateScore(3);
        Assert.assertEquals(3, player.score());
        player.updateScore(2);
        Assert.assertEquals(5, player.score());
    }


    @Test
    public void playerLoseScore() {
        player.updateScore(-3);
        Assert.assertEquals(-3, player.score());
        player.updateScore(-2);
        Assert.assertEquals(-5, player.score());
    }
}
